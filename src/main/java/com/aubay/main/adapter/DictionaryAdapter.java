package com.aubay.main.adapter;

import java.util.List;
import java.util.Map;

public interface DictionaryAdapter {
    /**
     * Optimize the list of words through a grouped list by initial letters
     * @return grouped list by initial letters
     */
    Map<String, List<String>> getWordsGroupedByLetter();
}