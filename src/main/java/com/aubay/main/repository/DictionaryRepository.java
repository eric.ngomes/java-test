package com.aubay.main.repository;

import java.util.List;
import java.util.Optional;

public interface DictionaryRepository {
    /**
     * Find the words that start by the text
     * @param words List of words
     * @param text Text to find
     * @return List of words matched
     */
    Optional<List<String>> startBy(List<String> words, String text);

    /**
     * Find a word that equals to
     * @param words List of words
     * @param word Word to find
     * @return A word matched
     */
    Optional<String> findByWord(List<String> words, String word);

    /**
     * Save a new word in the dictionary object
     * @param words List of words
     * @param word Word to save
     */
    void save(List<String> words, String word);

    /**
     * Update the word in the dictionary object
     * @param words List of words
     * @param oldWord Word to update
     * @param newWord New word to update
     */
    void update(List<String> words, String oldWord, String newWord);

    /**
     * Delete word in the
     * @param words List of words
     * @param word word to delete
     */
    void delete(List<String> words, String word);
}
